_Veronica, gimme a hand..._

![](veronica.gif)

Veronica is the Hulkbuster to getting a local SSBT Bottom Screen and local Angelia running on a developers machine. Getting everything setup manually (especially Angelia) is tedious and error prone. Veronica helps to eliminate that by doing it all for you.

## You Will Need

- A Macbook
- Git
- Java 8 (instructions can be found [here](https://angelia-williamhillplc-retail-applications-e77e40abdd780e4edea0.gitlab.io/Useful-Information/switching-java-versions/))
- Maven (mvn)
- Node 16 (instructions can be found [here](https://angelia-williamhillplc-retail-applications-e77e40abdd780e4edea0.gitlab.io/Useful-Information/switching-node-versions/))
- [IntelliJ 2019.3.4](https://bit.ly/3ft38m5) is recommended

## Getting Started
Scripts **MUST** be run as `sudo`!

### First Time?
First time installing all the repos you need for SSBT development?
```bash
sudo python3 veronica.py
```
And answer `y` to everything that needs to be installed.

### Pro 😎
Remove the folders of stuff you've already installed (e.g. remove `angelia` folder if you are gonna install angelia again).
```bash
sudo python3 veronica.py
```

Answer `y` to what you need. 
