import os
import pwd
import subprocess
import sys
import time


class colours:
    BLUE = "\033[94m"
    BOLD = "\033[1m"
    CYAN = "\033[96m"
    GREEN = "\033[92m"
    ORANGE = "\033[33m"
    PURPLE = "\033[35m"
    RED = "\033[91m"
    UNDERLINE = "\033[4m"
    WHITE = "\033[0m"
    YELLOW = "\033[93m"


logo = """
 __      __                  _
 \ \    / /                 (_)          
  \ \  / /__ _ __ ___  _ __  _  ___ __ _ 
   \ \/ / _ \ '__/ _ \| '_ \| |/ __/ _' |
    \  /  __/ | | (_) | | | | | (_| (_| |
     \/ \___|_|  \___/|_| |_|_|\___\__,_|
================================================
"""
intervals = (
    ("weeks", 604800),  # 60 * 60 * 24 * 7
    ("days", 86400),  # 60 * 60 * 24
    ("hours", 3600),  # 60 * 60
    ("minutes", 60),
    ("seconds", 1),
)


def display_time_taken(seconds, granularity=2):
    result = []
    for name, count in intervals:
        value = seconds // count
        if value:
            seconds -= value * count
            if value == 1:
                name = name.rstrip("s")
            result.append("{} {}".format(value, name))
    return ", ".join(result[:granularity])


def print_done():
    print(f"{colours.GREEN}Done!\n{colours.WHITE}")


def print_colour(colour, message):
    print(f"{colour}{message}{colours.WHITE}")


def ask_to_install(item):
    answer = input(f"Install {item}? (y/n) ")
    if answer == "y" or answer == "Y":
        return True
    return False


def cmd(cmd):
    subprocess.call(cmd, shell=True)


print_colour(colours.YELLOW, logo)
print(f"You will need:")
print_colour(colours.BLUE, "Git")
print_colour(colours.CYAN, "Maven")
print_colour(colours.RED, "Node 16")
print_colour(colours.ORANGE, "Java 8")
print_colour(colours.WHITE, "A copy of settings.xml from another developer. It should go in {colours.WHITE}{colours.BOLD} ~/.m2/settings.xml{colours.WHITE}")
print("\nAfter installing the above, restart your terminal window and run this script again.\n")

starting_directory = os.getcwd()
user = pwd.getpwuid(os.getuid())[0]
print(f"Veronica will install Apache Tomcat 8.0.46, Angelia, SSBT Bottom Screen, and the Message Broker to this location: \n{colours.GREEN}{starting_directory}{colours.WHITE}")
answer = input("Happy to install here, and got all the prerequisites above? (y/n) ")
if answer != "y" and answer != "Y":
    exit()

node_version = subprocess.getoutput("node -v")
java_version = subprocess.getoutput("java -version")

if node_version.startswith("v16") == False:
    print_colour(colours.RED, f"Incorrect Node version detected: {node_version}")
    exit()

if "1.8" not in java_version:
    print_colour(colours.RED, f"Incorrect Java version detected:\n{java_version}")
    exit()

print_colour(colours.GREEN, "\nHere we go!")
install_angelia = ask_to_install("Angelia")
install_tomcat = ask_to_install("Tomcat")
install_bottom_screen = ask_to_install("Bottom Screen")
install_top_screen = ask_to_install("Top Screen")
install_broker = ask_to_install(f"Messenger Broker ({colours.RED}MUST be installed for the screens and Angelia!{colours().WHITE})")

start = time.perf_counter()

# Angelia
if install_angelia:
    print_colour(colours.RED, "Setting up Angelia")
    cmd("git clone https://gitlab.com/williamhillplc/retail/applications/angelia.git")
    print_colour(colours.RED, "Cloning submodules")
    cmd("cd angelia && mkdir angelia-deployment && cd angelia-deployment && mkdir angelia-puppet && cd angelia-puppet")
    cmd("cd angelia/angelia-deployment/angelia-puppet && git clone https://gitlab.com/williamhillplc/retail/puppet/modules/angelia.git .")
    cmd("cd angelia && mvn clean install -DskipTests")
    print(colours.RED, "Creating your own filter properties file: {colours.WHITE}{colours.BOLD}src/main/filters/{user}.filter.properties{colours.WHITE}")
    command = f"cp ./angelia/src/main/filters/\\<user_name\\>.filter.properties ./angelia/src/main/filters/{user}.filter.properties"
    cmd(command)
    print(colours.RED, "Creating log directories")
    cmd("mkdir /var/log/angelia")
    cmd("touch /var/log/angelia/angelia.log")
    cmd("chmod 777 /var/log/angelia/angelia.log")
    print_done()

# Tomcat
if install_tomcat:
    print_colour(colours.PURPLE, "Downloading and extracting Tomcat 8.0.46")
    cmd("curl -sS https://archive.apache.org/dist/tomcat/tomcat-8/v8.0.46/bin/apache-tomcat-8.0.46.zip >tomcat.zip")
    cmd("unzip -q tomcat.zip")
    cmd("rm tomcat.zip")
    cmd(
        """cat > apache-tomcat-8.0.46/bin/setenv.sh <<- "EOF"
    export CATALINA_OPTS="-Ddiffusion.tls.protocols=TLSv1.2"
    EOF"""
    )
    print_done()

# Bottom Screen
if install_bottom_screen:
    print_colour(colours.BLUE, "Setting up SSBT Bottom Screen")
    cmd("git clone https://gitlab.com/williamhillplc/retail/applications/ssbt-bottom-screen.git")
    cmd("cd ssbt-bottom-screen && npm install")
    print_done()

# Top Screen
if install_top_screen:
    print_colour(colours.GREEN, "Setting up SSBT Top Screen")
    cmd("git clone https://gitlab.com/williamhillplc/retail/applications/ssbt-top-screen.git")
    cmd("cd ssbt-top-screen && npm install")
    print_done()

# Messenger Broker
if install_broker:
    print_colour(colours.ORANGE, "Setting up Messenger Broker")
    cmd("git clone https://gitlab.com/williamhillplc/retail/applications/ssbt-messenger-broker.git")
    cmd("cd ssbt-messenger-broker && npm install")
    print_done()

end = time.perf_counter()
time_taken = display_time_taken(end - start)
print(f"\n{colours.BOLD}Finished in: {time_taken}{colours.WHITE}\n")

print(f"Veronica has installed:")
if install_angelia:
    print(f"{colours.RED}Angelia")
    print(f"\t- Cloned the repo and its submodules")
    print(f"\t- mvn clean install has been run")
    print(f"\t- Created your own filter.properties file, use this for your own custom config in Angelia")
    print(f"\t- Created the correct log files in /var")
if install_tomcat:
    print(f"{colours.PURPLE}Tomcat 8.0.46")
    print(f"\t- Created a setenv.sh file")
if install_bottom_screen:
    print(f"{colours.BLUE}SSBT Bottom Screen")
    print(f"\t- Cloned the repo")
    print(f"\t- npm install has been run")
if install_top_screen:
    print(f"{colours.GREEN}SSBT Top Screen")
    print(f"\t- Cloned the repo")
    print(f"\t- npm install has been run")
if install_broker:
    print(f"{colours.ORANGE}Message Broker")
    print(f"\t- Cloned the repo")
    print(f"\t- npm install has been run")
print(f"{colours.WHITE}")

print(
    f"Follow the README instructions in the Angelia {colours.BOLD}(https://gitlab.com/williamhillplc/retail/applications/angelia){colours.WHITE} & SSBT Bottom Screen {colours.BOLD}(https://gitlab.com/williamhillplc/retail/applications/ssbt-bottom-screen){colours.WHITE} repos to get started."
)
print(f"Make sure your VPN is on, and that your config.js in SSBT Bottom Screen is pointing at the correct port of Angelia.")
